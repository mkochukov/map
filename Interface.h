//
//  Interface.h
//  Map
//
//  Created by Maxim Kochukov on 21/05/16.
//  Copyright (c) 2016 Maxim Kochukov. All rights reserved.
//

#include <string.h>

#ifndef Map_Interface_h
#define Map_Interface_h

template <class T_KEY, class T_VALUE>
class MapInterface {
    
    
public:
    
    //explicit MapInterface();
    
//---Operators:
    /*
    операторы присваивания возвращают тип самого объекта, не получается их реализовать в абстрактном классе. Выносить в реализацию.
    virtual MapInterface& operator= (const MapInterface & other);
    virtual MapInterface& operator= (const MapInterface && other);
    */
    
    virtual T_VALUE operator[] (const T_KEY key) const = 0;
    virtual T_VALUE& operator[] (const T_KEY key) = 0;
    
    
//---Iterators:
    
    /*
    
     * Итераторы не реализуются через абстрактный класс. Необходимо определять операторы для итератора, а их
     * реализация уникальна для каждой реализации. В то же время запрещено создавать виртуальные классы.
     
     Итераторы определять в каждом классе самостоятельно!
     
     const iterator begin() const;
     iterator begin();
     const iterator end() const;
     iterator end();
    */
    
    
//---Capacity:
    
    virtual bool Empty() const = 0;
    virtual size_t Size() const = 0;
    virtual size_t Max_Size() const = 0;
    
//---Modifiers:

    virtual void Insert(std::shared_ptr<T_KEY> key,std::shared_ptr<T_VALUE> value) = 0;
    virtual void Erase(std::shared_ptr<T_KEY> key) = 0;

};



#endif
