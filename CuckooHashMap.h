//
//  CuckooHashMap.h
//  Map
//
//  Created by Maxim Kochukov on 21/05/16.
//  Copyright (c) 2016 Maxim Kochukov. All rights reserved.
//

#ifndef __Map__CuckooHashMap__
#define __Map__CuckooHashMap__

#include <stdio.h>
#include <vector>
#include <algorithm>
#include "Interface.h"
#include <limits.h>

#define PRIME_1 100008731717
#define PRIME_2 957452939687

template <class T_KEY, class  T_VALUE>
class CuckooHashMap : public MapInterface<T_KEY,T_VALUE> {
private:
    struct Cell{
    public:
        std::shared_ptr<T_KEY> key;
        std::shared_ptr<T_VALUE> value;
        Cell(std::shared_ptr<T_KEY> key_, std::shared_ptr<T_VALUE> value_) : key(key_), value(value_){};
    };
    
    
    int *tuk;
    std::vector<std::shared_ptr<Cell>> table1;
    std::vector<std::shared_ptr<Cell>> table2;
    
    size_t tableSize;
    size_t size;
    
    size_t hash(T_KEY key,unsigned funno){
        std::hash<T_KEY> hash_fn;
        return (hash_fn(key) * ((funno == 1) ? PRIME_1 : PRIME_2)) % tableSize;
    };
    
    void insert( std::shared_ptr<T_KEY> key,  std::shared_ptr<T_VALUE> value, std::vector<std::pair<unsigned, size_t>>used){
        size_t h1 = hash(*key.get(),1), h2 = hash(*key.get(), 2);
        
        if (table1[h1] == NULL){
            table1[h1] = std::make_shared<Cell>(key,value);
            size++;
        }else if (table2[h2] == NULL){
            table1[h2] =  std::make_shared<Cell>(key,value);
            size++;
        }else{
            
            std::shared_ptr<Cell> holding = std::make_shared<Cell>(key,value);
            std::vector<std::pair<unsigned,size_t>> used;
            
            unsigned tno = 2;
            
            while (holding != nullptr){
                size_t h1 = hash(*holding->key.get(),1), h2 = hash(*holding->key.get(), 2);
                std::pair<unsigned,size_t> p1(1,h1),p2(2,h2);
                
                // проверим если нет такой
                if (std::find(used.begin(), used.end(), p1) != used.end() || std::find(used.begin(), used.end(), p2) != used.end()){
                    grow();
                    // insert holding
                    Insert(std::move(holding->key), std::move(holding->value));
                    return;
                }
                //если можно подставим
                else if (tno==1){
                    if (table1[h1] == nullptr){
                        std::swap(holding, table1[h1]);
                        size++;
                    }else{
                        std::swap(holding, table1[h1]);
                        tno = 2;
                        used.push_back({1,h1});
                    }
                } else if (tno == 2) {
                    if (table1[h2] == nullptr){
                        std::swap(holding, table2[h2]);
                        size++;
                    }else{
                        std::swap(holding, table2[h2]);
                        tno = 1;
                        used.push_back({2,h2});
                    }
                }
                //если нет поменяем
            }
            
        }
        
            
        
    }
    
    void grow(){
        tableSize *= 2;
        std::vector<std::shared_ptr<Cell>> temp(tableSize);
        std::for_each(table1.begin(), table1.end(), [&temp](std::shared_ptr<Cell> cell){
            if (cell != nullptr) temp.push_back(std::move(cell));
        });
        std::for_each(table2.begin(), table2.end(), [&temp](std::shared_ptr<Cell> cell){
            if (cell != nullptr) temp.push_back(std::move(cell));
        });
        table1.clear();
        table1.resize(tableSize);
        table2.clear();
        table2.resize(tableSize);
        
        std::for_each(table2.begin(), table2.end(), [this](std::shared_ptr<Cell> cell){
            Insert(cell->key, cell->value);
        });
        
    }
    
public:
    
    CuckooHashMap() : tableSize(8){
        table1.resize(8);
        table2.resize(8);
    };
    

    
    //---Operators:
    CuckooHashMap<T_KEY, T_VALUE>& operator= (const CuckooHashMap<T_KEY, T_VALUE> & other){
        table1 = other->table1;
        table2 = other->table2;
        size = other->size;
        tableSize = other->tableSize;
        return this;
    };
    CuckooHashMap<T_KEY, T_VALUE>& operator= (const CuckooHashMap<T_KEY, T_VALUE> && other){
        std::swap(table1, other,table1);
        std::swap(table2, other,table2);
        size = other->size;
        tableSize = other->tableSize;
        return this;
    };
    
    T_VALUE operator[] (const T_KEY key) const{
        size_t h1 = hash(*key.get(),1), h2 = hash(*key.get(), 2);
        if (table1[h1]->key == key){
            return table1[h1]->value.get();
            
        }else if (table2[h2]->key == key){
            return table1[h2]->value.get();
        }else{
            return nullptr;
        }
    }
    
    T_VALUE& operator[] (const T_KEY key){
        size_t h1 = hash(*key.get(),1), h2 = hash(*key.get(), 2);
        if (table1[h1]->key == key){
            return table1[h1]->value.get();
            
        }else if (table2[h2]->key == key){
            return table1[h2]->value.get();
        }else{
            return nullptr;
        }
    }
    
    
    //---Iterators:
    
    /*
     
     * Итераторы не реализуются через абстрактный класс. Необходимо определять операторы для итератора, а их
     * реализация уникальна для каждой реализации. В то же время запрещено создавать виртуальные классы.
     
     Итераторы определять в каждом классе самостоятельно!
     
     const iterator begin() const;
     iterator begin();
     const iterator end() const;
     iterator end();
     */
    
    
    //---Capacity:
    
    bool Empty() const {
        return (size==0);
    };
     size_t Size() const{
         return size;
     };
     size_t Max_Size() const{
         return tableSize;
     };
    
    //---Modifiers:
    
    void Insert(std::shared_ptr<T_KEY> key,std::shared_ptr<T_VALUE> value){
        std::vector<std::pair<unsigned,size_t>> used;
        insert(key, value, used);
    };
    
    void Erase(std::shared_ptr<T_KEY> key){
        size_t h1 = hash(*key.get(),1), h2 = hash(*key.get(), 2);
        if (table1[h1]->key == key){
            table1[h1] = nullptr;
            size--;
        }else if (table2[h2]->key == key){
            table1[h2] = nullptr;
            size--;
        }
        
    };
    
};
//Non-const lvalue reference to type 'int' cannot bind to a temporary of type shared_ptr


#endif /* defined(__Map__CuckooHashMap__) */
